.help sua2 Mar2001 WCSTools
.ih
NAME
sua2 -- Find USNO-A2.0 Catalog stars in a square on the sky
.ih
USAGE
sua2 [options] ra dec system
.ih
ARGUMENTS
.ls -a
list single closest catalog source
.le
.ls -b
Output B1950 (FK4) coordinates
.le
.ls -d
Sort by distance from center instead of flux
.le
.ls -e
Output ecliptic coordinates
.le
.ls -f
Output search center for other programs
.le
.ls -g
Output galactic coordinates
.le
.ls -h
Print heading, else do not 
.le
.ls -i
Print catalog object name, not catalog number (=ua2_region_number)
.le
.ls -j
Output J2000 (FK5) coordinates
.le
.ls -l
Print center and closest star on one line
.le
.ls -m [<bright magnitude>] <faint magnitude>
Limiting catalog magnitude(s) (default none, bright -2 if only faint is given)
.le
.ls -n <num>
Number of brightest stars to print 
.le
.ls -o <name>
Object name used to name output file
.le
.ls -p
Sort by distance from search center instead of brightness
.le
.ls -q <years>
Equinox of output positions in years
.le
.ls -r <radius>
Search box half-width in arcsec or dd:mm:ss
If negative, this is a radius; if two numbers, first is in right ascension,
second is in declination. (default is 10, that is a 20x20 arcsec box)
.le
.ls -s
Sort by right ascension instead of flux 
.le
.ls -t
Tab table output
.le
.ls -u <num>
Print X Y instead of number in front of non-tab entry
.le
.ls -v
Verbose listing of processing intermediate results
.le
.ls -w
Write tab table output file imagename.uac
.le
.ls -y
Epoch of output positions in years
.le
.ih
DESCRIPTION
.I sua2
finds all of the U.S. Naval Observatory A2.0 Catalog objects in a specified
region of the sky and lists their sky positions and magnitudes in order of
brightness. Output is to standard out, unless the -w flag is set, in which
case it goes to objectname.uac or search.uac. It is a link to scat.
.ih
WEB PAGE
http://tdc-www.harvard.edu/software/wcstools/scat/sua2.html
.ih
SEE ALSO
scat, imcat, imua2
.ih
AUTHOR
Jessica Mink, SAO (jmink@cfa.harvard.edu)
.endhelp
