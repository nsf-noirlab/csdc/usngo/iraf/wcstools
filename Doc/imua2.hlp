.help imua2 Mar2001 WCSTools
.ih
NAME
imua2 \- Find USNO-A2.0 stars in FITS or IRAF image files
.ih
USAGE
imua2 [options] FITS or IRAF file(s)
.ih
ARGUMENTS
.ls -b [<RA> <Dec>]
Output in B1950, optional new image center in B1950 (FK4) RA and Dec
.le
.ls -h
Print heading, else do not 
.le
.ls -j [<RA> <Dec>]
Output in J2000, optional new image center in J2000 (FK5) RA and Dec
.le
.ls -m [<bright magnitude>] <faint magnitude>
Limiting catalog magnitude(s) (default none, bright -2 if only faint is given)
.le
.ls -n <num>
Number of brightest stars to print 
.le
.ls -p <scale>
Initial plate scale in arcsec per pixel (default 0)
.le
.ls -s
Sort by RA instead of flux 
.le
.ls -t
Tab table to standard output as well as file
.le
.ls -u
USNO catalog single plate number to accept
.le
.ls -v
Verbose listing of processing intermediate results
.le
.ls -w
Write tab table output file imagename.cat
.ls -z
Use AIPS classic projections instead of WCSLIB; use CD matrix or CDELT
instead of polynomial solution.
See Also
imgsc(),imuac(),imusac(),scat()
Author
Jessica Mink, SAO (jmink@cfa.harvard.edu)
Options
.le
.ls -b [<RA> <Dec>]
Output B1950 (FK4) coordinates (optional center)
.le
.ls -u <plate>
Accept only stars from this plate (default all)
.le
.ls -h
Print heading, else do not 
.le
.ls -j [<RA> <Dec>]
Output J2000 (FK5) coordinates (optional center)
.le
.ls -m [<bright magnitude>] <faint magnitude>
Limiting catalog magnitude(s) (default none, bright -2 if only faint is given)
.le
.ls -n <num>
Number of brightest stars to print 
.le
.ls -p <scale>
Plate scale in arcsec per pixel (default 0)
.le
.ls -s
Sort by RA instead of flux 
.le
.ls -t
Tab table to standard output as well as file
.le
.ls -u <plate>
Accept only stars from thsi plate (default all)
.le
.ls -v
Verbose listing of processing intermediate results
.le
.ls -w
Write tab table output file imagename.uac
.le
.ih
DESCRIPTION
Search USNO-A2.0 Catalog within the area described by the world coordinate
system in an image header.  This is a link to imcat rather than
a separate executable.
.ih
SEE ALSO
imcat, imgsc, imusa2, scat, sua2
.ih
WEB PAGE
http://tdc-www.harvard.edu/software/wcstools/imcat/imua2.html
.ih
AUTHOR
Jessica Mink, SAO (jmink@cfa.harvard.edu)
.endhelp
